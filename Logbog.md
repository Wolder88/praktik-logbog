## Logbog for Niels Frederiksen

### Uge 1 // d. 26 januar
I denne uge har jeg fået adgang til diverse systemer og påbegyndt arbejdet med et pythonprogram den kan forbinde med API keys til både Units database-system og Shodan.io, som er en search engine der kan bruges til at finde servere af mistænkelig karakter. Jeg er efterfølgende begyndt at logge data fra Shodan over i databasen, hvor vi særligt leder efter servere der har et Jarm-fingerprint der matcher cobalt-strike Command-and-Control servere. Formålet er at lave et pythonscript der er på forkant med nye malicious Cobalt Strike adresser og kan spærre dem, inden de foretager skadelig aktivitet. 

### Uge 2 // d. 02 februar
I denne uge har jeg arbejdet videre med Python-integration af Shodan. Der er blevet opsat og mappet forbindelser mellem 3 forskellige databaser og 2 python programmer, mens flere søge kriterier og signaturer til Shodan er blevet udvalgt. 

### Uge 3 // d. 09 februar
I denne uge har jeg færdiggjort python-modulerne, og den store datahøst er statet. Vi har fundet over 300 Cobalt Strike Beacons, som nu er logget i SQL-databaserne og kan analyseres. På sigt skal denne data integreres i virksomhedens Open CTI-client, hvor vi bruger dataformatet "STIX", hvor jeg er begyndt at læse op på dette.

### Uge 4 // d. 16 februar
I denne uge har jeg arbejdet i Docker, med henblik på at få pythonprojektet til at køre i en container. Jeg har også flyttet koden over i et virutelt linux ubuntu-miljø, hvilket gav nogle udfordringer, da det tidligere OS var Windows. Jeg er også så småt begyndt på python-koden, der skal fungere som "connector" fra SQL-databasen over til openCTI klienten. Her lykkedes det at få forbindelse med API-nøglen og selve koden skal derfor nu videreudvikles. 

### Uge 5 // d. 23 februar
I denne uge lykkedes det at få forbindelse og overføre data fra SQL-serveren hele vejen til openCTI-testmiljøet. Det betyder at næste skridt er at overføre rigtig data, fremfor Mock-data. Samtidigt skal data formateres korrekt, således at forhold og relationer er repræsenteret korrekti opennCTI, efte en overførsel. Det har jeg holdt en række møder med brugerne af openCTI omkring. Underviser fra UCL fra også forbi adressen i Middelfart til et statusmøde. 

### Uge 6 // d. 01. marts 
Kæmpe gennembrud! Stellarsense-projektet er i mål og rigtig data kan nu rejse hele vejen fra en Shodan-search quary over i openCTI-platformen og visualiseres. Samtlige moduler er automatiserede. Interne relationer i STIX2.1 objekterne er korrekt repræsenteret og visualiseringen matcher standarden på platformen. I denne uge fik jeg også en mindre intro til Q-radar og Veliciraptor, der anvendes som henholdsvis SIEM og incident response værktøjer. 

### Uge 7 // d. 08 marts
I denne uge blev stellarsense-projketet stresstestet og debugged. Samtidigt var der en tema-tur til Ballerup, hvor vi snakkede business intelligence, purple teaming og modenhedsvurdering ift. ISO-standarder og NIST 2 hos kunder.